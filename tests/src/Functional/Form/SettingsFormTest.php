<?php

namespace Drupal\Tests\config_view\Form;

use Drupal\Tests\BrowserTestBase;

/**
 * Settings form test.
 *
 * @group config_view
 */
class SettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['config_view', 'block'];

  /**
   * The test user.
   *
   * @var \Drupal\User\UserInterface
   */
  protected $admin;

  /**
   * Test the settings form submits properly.
   */
  public function testForm() {
    $this->admin = $this->createUser([], NULL, TRUE);
    $this->drupalLogin($this->admin);

    $this->drupalGet('/admin/structure/views/settings/config_view');

    $edit = [
      'edit-data-block' => TRUE,
    ];
    $this->drupalPostForm(NULL, $edit, t('Submit'));
    $config = \Drupal::config('config_view.settings')->get('data');
    $this->assertTrue('block', $config['block']);
  }

}
